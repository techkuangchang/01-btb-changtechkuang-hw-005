package com.kshrd.homework005.services;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.kshrd.homework005.models.BookModel;
import com.kshrd.homework005.models.CategoryModel;

import java.util.List;

@Dao
public interface BookService {

    @Query("SELECT *FROM tb_book")
    List<BookModel> findAll();

    @Insert
    void insertBook(BookModel bookModel);

    @Query("DELETE FROM tb_book WHERE bookId=:bookId")
    void deleteBookById(int bookId);

    @Query("SELECT *FROM tb_book WHERE bookId=:bookId")
    List<BookModel> findById(int bookId);

//    @Update
//    void updateBookById(int id, BookModel bookModel);
}
