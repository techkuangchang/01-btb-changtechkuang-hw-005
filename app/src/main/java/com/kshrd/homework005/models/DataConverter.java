package com.kshrd.homework005.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import androidx.room.TypeConverter;

import java.io.ByteArrayOutputStream;

public class DataConverter {

    public static byte[] convertImageToByteArray(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        if(temp == null){
            return null;
        }
        else {
            return temp;
        }
    }

    @TypeConverter
    public static Bitmap convertByteArrayToImage(byte[] array){
//        try {
//            byte[] encodeByte = Base64.decode(image, Base64.DEFAULT);
//            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
//            if(bitmap == null){
//                return null;
//            }
//            else {
//                return bitmap;
//            }
//        }catch (Exception e){
//            e.getMessage();
//            return null;
//        }
        return BitmapFactory.decodeByteArray(array, 0, array.length);
    }

    public static Bitmap StringToBitMap(String encodedString){
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            if(bitmap == null){
                return null;
            }
            else {
                return bitmap;
            }
        }catch (Exception e){
            e.getMessage();
            return null;
        }
    }

}
