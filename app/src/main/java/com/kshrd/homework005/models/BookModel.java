package com.kshrd.homework005.models;

import android.net.Uri;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "tb_book")
public class BookModel {

    @PrimaryKey(autoGenerate = true)
    private int bookId;
    private String bookName;
    private double bookSize;
    private double bookPrice;
    private String bookImage;

    @ForeignKey(
            entity = CategoryModel.class,
            parentColumns = "categoryId",
            childColumns = "categoryId_fk"
    )
    private int categoryId_fk;

    public BookModel(){}

    public BookModel(String bookImage, String bookName, double bookSize, double bookPrice, int categoryId_fk) {
        this.bookImage = bookImage;
        this.bookName = bookName;
        this.bookSize = bookSize;
        this.bookPrice = bookPrice;
        this.categoryId_fk = categoryId_fk;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getBookImage() {
        return bookImage;
    }

    public void setBookImage(String bookImage) {
        this.bookImage = bookImage;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public double getBookSize() {
        return bookSize;
    }

    public void setBookSize(double bookSize) {
        this.bookSize = bookSize;
    }

    public double getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(double bookPrice) {
        this.bookPrice = bookPrice;
    }

    public int getCategoryId_fk() {
        return categoryId_fk;
    }

    public void setCategoryId_fk(int categoryId_fk) {
        this.categoryId_fk = categoryId_fk;
    }

    @Override
    public String toString() {
        return "BookModel{" +
                "bookId=" + bookId +
                ", bookImage=" + bookImage +
                ", bookName='" + bookName + '\'' +
                ", bookSize=" + bookSize +
                ", bookPrice=" + bookPrice +
                ", categoryId_fk=" + categoryId_fk +
                '}';
    }
}
