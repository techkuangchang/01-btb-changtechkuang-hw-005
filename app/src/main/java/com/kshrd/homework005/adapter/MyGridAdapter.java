package com.kshrd.homework005.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.TypeConverter;

import com.kshrd.homework005.R;
import com.kshrd.homework005.databaseConfig.ConfigurationDB;
import com.kshrd.homework005.models.BookModel;
import com.kshrd.homework005.models.DataConverter;

import java.util.List;

public class MyGridAdapter extends RecyclerView.Adapter<MyGridAdapter.ViewHolder> {

    private Context context;
    private List<BookModel> bookModelList;

    public MyGridAdapter(Context context, List<BookModel> bookModelList){
        this.context = context;
        this.bookModelList = bookModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_custom_gird_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyGridAdapter.ViewHolder holder, final int position) {
        BookModel bookModel = bookModelList.get(position);
        holder.imageViewBook.setImageBitmap(StringToBitMap(bookModel.getBookImage()));
        holder.txtBookName.setText(bookModel.getBookName());
        holder.txtBookSize.setText(String.valueOf(bookModel.getBookSize()));
        holder.txtBookPrice.setText(String.valueOf(bookModel.getBookPrice()));
    }

    @Override
    public int getItemCount() {
        return bookModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageViewBook;
        TextView txtBookName, txtBookSize, txtBookPrice;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewBook = itemView.findViewById(R.id.my_custom_layout_image_view);
            txtBookName = itemView.findViewById(R.id.my_custom_layout_txt_book_name);
            txtBookSize = itemView.findViewById(R.id.my_custom_layout_txt_size);
            txtBookPrice = itemView.findViewById(R.id.my_custom_layout_txt_price);
        }
    }

    @TypeConverter
    public Bitmap StringToBitMap(String image){
        try {
            byte[] encodeByte = Base64.decode(image, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            if(bitmap == null){
                return null;
            }
            else {
                return bitmap;
            }
        }catch (Exception e){
            e.getMessage();
            return null;
        }
    }
}
