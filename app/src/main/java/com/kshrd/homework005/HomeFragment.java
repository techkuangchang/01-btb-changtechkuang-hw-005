package com.kshrd.homework005;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.TypeConverter;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.kshrd.homework005.adapter.MyGridAdapter;
import com.kshrd.homework005.databaseConfig.ConfigurationDB;
import com.kshrd.homework005.models.BookModel;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private GridLayoutManager gridLayoutManager;
    private Toolbar mToolbar, container_side_nav1;

    List<BookModel> bookModelList;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.home_fragment_recyclerview, container, false);
        mToolbar = view.findViewById(R.id.container_toolbar);
        mToolbar.setTitle(R.string.home);

        //Room is here
        bookModelList = ConfigurationDB.getDatabase(getActivity()).bookService().findAll();
        Log.d("TAG FindAll", String.valueOf(bookModelList));

//        bookModelList = ConfigurationDB.getDatabase(getActivity()).bookService().findById(bookModelList.get())

        mRecyclerView = view.findViewById(R.id.container_recycler_view);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        mAdapter = new MyGridAdapter(getActivity(), bookModelList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);

        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
//        getActivity().getMenuInflater().inflate(R.menu.menu, menu);
//        MenuInflater menuInflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.side_bar_menu,menu);
        //Search ToolBar
//        MenuItem menuItem = menu.findItem(R.id.menu_search_bar);
//        SearchView searchView = (SearchView) menuItem.getActionView();
//        searchView.setQueryHint("Type the book's name");
//
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                return true;
//            }
//        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.side_nav_read) {
            Toast.makeText(getActivity(), "READ", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
