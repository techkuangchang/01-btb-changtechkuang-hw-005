package com.kshrd.homework005.databaseConfig;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.os.IResultReceiver;
import android.util.Base64;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;

import com.kshrd.homework005.models.BookModel;
import com.kshrd.homework005.services.BookService;

import java.io.ByteArrayOutputStream;

@Database(entities = {BookModel.class}, version = 1)
public abstract class ConfigurationDB extends RoomDatabase {

    public abstract BookService bookService();
    private static ConfigurationDB INSTANCE;

    public static ConfigurationDB getDatabase(Context context){
        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context, ConfigurationDB.class, "tb_book")
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

}
