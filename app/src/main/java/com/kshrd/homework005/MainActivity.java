package com.kshrd.homework005;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.room.TypeConverter;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.kshrd.homework005.databaseConfig.ConfigurationDB;
import com.kshrd.homework005.models.BookModel;
import com.kshrd.homework005.models.DataConverter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Toolbar mToolbar, mSideNavRead;
    BottomNavigationView bottomNavigationView;
    FloatingActionButton floatingActionButton;
    ImageView mImageBook;
    Bitmap bmpImage;
    Button mBtnSave, mBtnCancel;
    EditText mEditBookName, mEditBookSize, mEditBookPrice;
    Spinner mSpinnerCategory;

    String permissions[];
    static final int PERMISSION_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mImageBook = findViewById(R.id.Insert_ImageView);
//        mSideNavRead = findViewById(R.id.side_nav_read);
        bmpImage = null;

        //Initialization Toolbar
        mToolbar = findViewById(R.id.toolbar_nav);
        setSupportActionBar(mToolbar);

        //Initialization Bottom Navigation
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                switch (item.getItemId()){
                    case R.id.bottom_nav_home :
                        mToolbar.setTitle(R.string.home);
                        fragment = new HomeFragment();
                        break;
                    case R.id.bottom_nav_dashboard :
                        mToolbar.setTitle(R.string.dashboard);
                        fragment = new DashboardFragment();
                        break;
                    case R.id.bottom_nav_notification :
                        mToolbar.setTitle(R.string.notification);
                        fragment = new NotificationFragment();
                        break;
                    case R.id.bottom_nav_upload :
                        mToolbar.setTitle(R.string.upload);
                        fragment = new UploadFragment();
                        break;
                    case R.id.bottom_nav_share :
                        mToolbar.setTitle(R.string.share);
                        fragment = new ShareFragment();
                        break;
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.container_layout, fragment).commit();
                return true;
            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.container_layout,new HomeFragment()).commit();
    }

    /*
    Handle SearchBar
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        //Search ToolBar
        MenuItem menuItem = menu.findItem(R.id.menu_search_bar);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint("Type the book's name");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
        return true;
    }

    /*
    Handle ToolBar
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_search_bar :
                System.out.println("You Click Menu Search");
                break;
            case R.id.menu_add_bar :
                System.out.println("You Click Menu Add");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //OnClick Add Method
    public void toolbar_add(MenuItem item) {
        ViewGroup viewGroup = findViewById(R.id.content);
        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.custom_dialog, viewGroup, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        //Method Add New Book
        mBtnCancel = view.findViewById(R.id.insert_book_cancel);
        mBtnSave = view.findViewById(R.id.insert_book_save);
        mEditBookName = view.findViewById(R.id.edit_book_name);
        mEditBookSize = view.findViewById(R.id.edit_book_size);
        mEditBookPrice = view.findViewById(R.id.edit_book_price);
        mSpinnerCategory = view.findViewById(R.id.spinner);
        mImageBook = view.findViewById(R.id.Insert_ImageView);

        floatingActionButton = view.findViewById(R.id.floating_add_btn);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogAdd(v);
            }
        });

        /*
        SAVE BOOK
         */
        final ConfigurationDB instance = ConfigurationDB.getDatabase(this);
        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookModel bookModel = new BookModel();

//                if(mImageBook == null || mEditBookPrice == null || mEditBookSize == null || mEditBookName == null){
//                    Toast.makeText(getApplicationContext(),"Please Input a textbox",Toast.LENGTH_SHORT).show();
//                }

                if (instance != null) {
                    bookModel.setBookImage(DataConverter.BitMapToString(bmpImage));
                    bookModel.setBookName(mEditBookName.getText().toString());
                    bookModel.setBookPrice(Double.parseDouble(mEditBookPrice.getText().toString()));
                    bookModel.setBookSize(Double.parseDouble(mEditBookPrice.getText().toString()));
                    bookModel.setCategoryId_fk(2);
                    instance.bookService().insertBook(bookModel);
                    alertDialog.dismiss();
                }
                else {
                    alertDialog.dismiss();
                }

            }
        });

        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }

    public void DialogAdd(View v){
        //Check Runtime Permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                //permission not graded, request it
                String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                //show popup for runtime permission
                requestPermissions(permissions, PERMISSION_CODE);
            }
            else {
                //permission already granted
                pickImageFromGallary();
            }
        }
        else {
            //system os is less than marshmallow
            pickImageFromGallary();
        }
    }

    private void pickImageFromGallary() {
        //intent to pick image
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PERMISSION_CODE :
                if (grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //permission was granted
                    pickImageFromGallary();
                }
                else {
                    //permission was denied
                    Toast.makeText(this,"Permission denied...!", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                bmpImage = BitmapFactory.decodeStream(inputStream);
                if (bmpImage != null) {
                    mImageBook.setImageBitmap(bmpImage);
                } else {
                    Toast.makeText(this, "Bitmap is NULL", Toast.LENGTH_SHORT).show();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }


}